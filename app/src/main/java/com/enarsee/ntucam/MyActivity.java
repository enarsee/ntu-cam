package com.enarsee.ntucam;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase;


public class MyActivity extends Activity implements ActionBar.OnNavigationListener {
    ImageViewTouch mImage;
    /**
     * The serialization (saved instance state) Bundle key representing the
     * current dropdown position.
     */
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private static final String[] urls = {"adm","ntu-nie","StudyZoneoutsideSAC","lwn-inside","quad","WalkwaybetweenNorthAndSouthSpines","foodcourt","fastfood","canteenB"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        // Set up the action bar to show a dropdown list.
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        String[] names = {"Art Design and Media","Canopy K","Study Zone Outside SAC","Lee Wee Nam Library","SBS Quad","North and South Spine Walkway","Food Connection Canteen A","North Spine Fastfood","Canteen B"};




        // Set up the dropdown list navigation in the action bar.
        actionBar.setListNavigationCallbacks(
                // Specify a SpinnerAdapter to populate the dropdown list.
                new ArrayAdapter<String>(
                        actionBar.getThemedContext(),
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        names),
                this);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore the previously serialized current dropdown position.
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getActionBar().setSelectedNavigationItem(
                    savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Serialize the current dropdown position.
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM,
                getActionBar().getSelectedNavigationIndex());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);


        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item= menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(int position, long id) {
        // When the given dropdown item is selected, show its contents in the
        // container view.

        setContentView(R.layout.fragment_my);

        String name = urls[position];

           String url = "http://webcam.ntu.edu.sg/upload/slider/"+name+".jpg";



            CallAPI async = new CallAPI(this);
            async.execute(url);










      /*  getFragmentManager().beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();*/
        return true;
    }



    private class CallAPI extends AsyncTask<String, String, String> {

        private Context mContext;

        public CallAPI(Context context) {
            mContext = context;
        }

        ProgressDialog pd;

        @Override
        protected String doInBackground(String... params) {

            String urlString = params[0];


            // HTTP
            try {
                URL url = new URL(urlString);
                Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                displayMessage d = new displayMessage();
                d.image = image;

                MyActivity.this.runOnUiThread(d);

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }



        protected void onPreExecute()
        {
            pd= ProgressDialog.show(mContext, "", "Please Wait", false);
        }
        protected void onPostExecute(String result) {
            pd.dismiss();
        }

    }
    private class displayMessage implements Runnable {

        public Bitmap image;
        public String tag;

        @Override
        public void run() {
            it.sephiroth.android.library.imagezoom.ImageViewTouch a = (it.sephiroth.android.library.imagezoom.ImageViewTouch) findViewById(R.id.image);

         // ImageView result = (ImageView) findViewById(R.id.imageView);
          a.setImageBitmap(image);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            int total = width * height;
            float ratio = (float)(3.85/(1184*720))*total;

            Log.w("SIZE",Integer.toString(size.y));
            a.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);
            a.setScaleType(ImageView.ScaleType.MATRIX);

           // a.zoomTo(ratio,5);



            a.setScaleEnabled(false);



        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container, false);
            return rootView;
        }
    }

}
